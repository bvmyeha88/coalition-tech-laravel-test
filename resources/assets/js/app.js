import $ from 'jquery';
window.$ = window.jQuery = $;

/*Notification*/
function notification(type, msg, title) {
    if (type !== undefined) {
        var i = -1;
        var toastCount = 0;
        var $toastlast;
        var shortCutFunction = type;
        var msg = msg;
        var title = title;
        var toastIndex = toastCount++;
        var addClear = $('#addClear').prop('checked');

        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: false,
            progressBar: true,
            preventDuplicates: false,
            onclick: null,
            hideDuration: 1000,
            showDuration: 300,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut',
            tapToDismiss: false
        };
        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;
    }
}