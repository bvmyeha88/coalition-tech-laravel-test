<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{isset($title) ? $title : 'Coalition Technologies'}}</title>
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link href="{{ mix('/css/app.css') }}" rel="stylesheet">	

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	<!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
            .content-center{
            	margin: 0 auto;
            	max-width: 80%;
            }
        </style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Coalition Technologies</a>
        </div>
    </nav>
	@yield('content')

	<!-- Scripts -->
	<script src="{{mix('/js/app.js')}}"></script>
	<script type="text/javascript">
	    $(document).ready(function () {
	        var type = '';
	        var msg = '';
	        var title = 'Message';
	        @if(Session::has('flashInfo'))
	                type = "{{ Session::get('alert-class', 'info') }}";
	                msg = "{{ Session::get('flashInfo') }}";
	                notification(type, msg, title);
	                {{Session::forget('flashInfo')}};
	        @elseif(Session::has('flashSuccess'))
	                type = "{{ Session::get('alert-class', 'success') }}";
	                msg = "{{ Session::get('flashSuccess') }}";
	                notification(type, msg, title);
	                {{Session::forget('flashSuccess')}};
	        @elseif(Session::has('flashWarning'))
	                type = "{{ Session::get('alert-class', 'warning') }}";
	                msg = "{{ Session::get('flashWarning') }}";
	                notification(type, msg, title);
	                {{Session::forget('flashWarning')}};
	        @elseif(Session::has('flashError'))
	                type = "{{ Session::get('alert-class', 'error') }}";
	                msg = "{{ Session::get('flashError') }}";
	                notification(type, msg, title);
	                {{Session::forget('flashError')}};
	        @endif
	    });
	</script>
	@yield('script')
</body>
</html>