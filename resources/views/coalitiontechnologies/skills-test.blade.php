@extends('layouts.default')
@section('content')
<div class="container">

  <!-- Main component for a primary marketing message or call to action -->
  	<div class="jumbotron">
	    <h3>Product List</h3>
  	</div>
  	<div class="row content-center">
  		<form method="POST" id="product-form">
  			@csrf
  			<input type="hidden" class="product-id" name="id" value="{{$id}}">
  			<input type="hidden" class="is-edit" name="edit" value="0" />
		 	<div class="form-group col-sm-12">
		    	<label for="name">Product name</label>
		    	<input type="text" class="form-control" id="name" name="name" placeholder="Product Name">
		  	</div>
		  	<div class="form-group col-sm-12">
		    	<label for="quantity">Quantity In Stock</label>
		    	<input type="number" class="form-control" id="quantity" name="quantity" placeholder="0" min="0">
		  	</div>
		  	<div class="form-group col-sm-12">
		    	<label for="price">Price per Item</label>
		    	<input type="number" class="form-control" id="price" name="price" placeholder="0" min="0" step="0.01">
		  	</div>

		  	<div class="form-group col-sm-12">
		  		<button type="submit" class="btn btn-primary">Submit</button>
		  	</div>
		</form>	
  	</div>
  	<div class="row table-product content-center">
  		<table class="table table-hover">
		  	<thead>
			    <tr>
			      	<th scope="col">Product Name</th>
			      	<th scope="col">Quantity In Stock</th>
			      	<th scope="col">Price per Item</th>
			      	<th scope="col">Datetime submitted</th>
			      	<th scope="col">Total value number</th>
			      	<th scope="col">Action</th>
			    </tr>
		  	</thead>
		  	<tbody id="product-table">
		  		<?php 
		  			$total = 0; 
		  		?>
		  		@foreach( $products as $index => $product )
		  			<tr class="row-{{$product->id}}">
				    	<td class="col-name">{{$product->name}}</td>
				    	<td class="col-quantity">{{$product->quantity}}</td>
				    	<td class="col-price">{{$product->price}}</td>
				    	<td>{{$product->created_at}}</td>
				    	<td class="col-total">{{$product->quantity*$product->price}}</td>
				    	<td><a class="edit-product" href="#" data-product-id="{{$product->id}}" >Edit</a></td>
				    	<?php $total += $product->quantity*$product->price; ?>
			    	</tr>
		  		@endforeach
		    	<tr>
		      		<td colspan="6" class="text-center">Summary</td>
		      		<td class="sum">{{$total}}</td>
		    	</tr>
		  	</tbody>
		</table>
  	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function () {
		$.date = function(dateObject) {
		    var d = new Date(dateObject);
		    var day = d.getDate();
		    var month = d.getMonth() + 1;
		    var year = d.getFullYear();

		    if (day < 10) {
		        day = "0" + day;
		    }
		    if (month < 10) {
		        month = "0" + month;
		    }
		    var date = year + '-' + month + '-' + day;

		    var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

		    return date + ' ' + time;
		};
		var countProducts = {{count($products)}};
		$("#product-form").submit(function(e) {
		    var url = "{{route('product.submit')}}";
		    $.ajax({
		           type: "POST",
		           url: url,
		           data: $("#product-form").serialize(), // serializes the form's elements.
		           success: function(data)
		           {
		           		var nextId = data['nextId'];
		           		var product = data['request'];
		           		var isEdit = parseInt(data['edit']);
		           		var total =  parseInt(product['quantity']) * parseFloat(product['price']);
		           		if(isEdit){
		           			$clsName = $('.row-'+data['request']['id']);
		           			$clsName.find('td.col-name').text(product['name']);
		           			$clsName.find('td.col-price').text(product['price']);
		           			$clsName.find('td.col-quantity').text(product['quantity']);
		           			$clsName.find('td.col-total').text(total);
		           		} else {
			           		var submittedDate = new Date();
			           		var insertHtml = '<tr class="row-'+product['id']+'">';
			           		insertHtml += '<td class="col-name">'+ product['name'] +'</td>';
			           		insertHtml += '<td class="col-quantity">'+ product['quantity'] +'</td>';
			           		insertHtml += '<td class="col-price">'+ product['price'] +'</td>';
			           		insertHtml += '<td>'+ $.date(submittedDate) +'</td>';
			           		insertHtml += '<td class="col-total">'+ total +'</td>';
			           		insertHtml += '<td><a class="edit-product" href="#" data-product-id="' + product['id'] + '">' + 'Edit' +'</a></td>';
			           		insertHtml += '</tr>';
			           		$('#product-table').prepend(insertHtml);
		           		}

		           		// Update total
		           		var sum = $('.sum').text();
		           		sum = parseFloat(sum) + parseFloat(total); 
		           		$('.sum').text(sum);

		           		// Update product id
			           	$('.product-id').val(nextId);

			           	$('#product-form').trigger("reset");
			           	$('.is-edit').attr('value', 0);
		           		alert('submitted');

		           },
		           error: function() 
		           { 
		           		alert('Error occurred');
		           }
		         });

		    e.preventDefault();
		});

		$('#product-table').on('click', '.edit-product', function() {
			var productId = $(this).attr('data-product-id');

			$clsName = $('.row-'+productId);
   			var name = $clsName.find('td.col-name').text();
   			var price = $clsName.find('td.col-price').text();
   			var quantity = $clsName.find('td.col-quantity').text();
			
			// Update form
			$('#name').val(name);
			$('#price').val(price);
			$('#quantity').val(quantity);
			$('.product-id').val(productId);

			$('.is-edit').attr('value', 1);
		})
	});

</script>
@endsection