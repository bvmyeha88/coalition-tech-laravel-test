<?php namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use File;
use Request;

class ProductController extends Controller {
	/**
	 * Create a new SongController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}
	/**
	 * Show all products (GET).
	 *
	 * @return Response
	 */
	public function index()
	{
		$filePath = public_path().'/products.json';
		$products = [];
		if ( File::exists($filePath )){
			$products = json_decode(file_get_contents($filePath));
			if(isset($products->products)){
				$products = $products->products;
			}
		}

		$id = count($products) + 1;
		return view('coalitiontechnologies/skills-test', compact('products', 'id'));
	}
	/**
	 * Show form to add a new product (GET).
	 *
	 * @return Response
	 */
	public function createOrEdit()
	{	
		$rules = [
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ];

		$request = Input::all();
		$isEdit = intval($request['edit']);
		
		unset($request['_token']);
		unset($request['edit']);

		$validator = Validator::make($request, $rules);
		if ($validator->fails()) {
			$code = 400;
			$arrayResult = array(
	            'code' => $code,
	            'errorMessage' => "Missing parameters"
	        );
	        return response()->json($arrayResult, $code);
        }

		$filePath = public_path().'/products.json';
		$products = [];

		if(Request::ajax()) {
			
			if ( ! File::exists($filePath) ){
				$products = [
					'products' => $request
				];	
				$nextId = $request['id'] + 1;
				file_put_contents($filePath, json_encode($products) );
			}else{
				$oldProducts = json_decode(file_get_contents($filePath));
				if($isEdit){
					foreach($oldProducts->products as $key => $p) {
						if($p->id == $request['id']){
							$createdAt = $oldProducts->products[$key]->created_at;
							$request['created_at'] = $createdAt;
							$oldProducts->products[$key] = $request;
						}
						$maxId = $p->id;
						if($maxId < $p->id){
							$maxId = $p->id;
						}
					}
					$nextId = $maxId+1;
				} else {
					$request['created_at'] = date('Y-m-d H:i:s');
					array_unshift($oldProducts->products, $request);
					$nextId = $request['id'] + 1;
				}

				File::put($filePath, json_encode( $oldProducts ));
				
			}

			$result = array(
				'edit' => $isEdit ? 1 : 0,
				'nextId' => $nextId,
				'request' => $request
			);

			return response()->json($result);
		} else {
			abort(404);
		}
	}
}