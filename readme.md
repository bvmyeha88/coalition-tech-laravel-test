## Coalition Technologies Laravel Test

This is repository for Colation Techonologies Laravel Test.

# Questions:
- The submitted data of the form should be saved in an XML / JSON file with valid XML / JSON syntax. 
- Underneath of the form, the web page should display all of the data which has been submitted in rows ordered by date time submitted, the order of the data columns should be: Product name, Quantity in stock, Price per item, Datetime submitted, Total value number.
- The "Total value number" should be calculated as (Quantity in stock * Price per item).
- The last row should how a sum total of all of the Total Value numbers.
- For extra credit, include functionality to edit each line.


# Solution requirements:
- Use Php / Html / Javascript / CSS. Use Twitter Bootstrap.
- The form should be submitting the data and updating the data being displayed on the page using Ajax.
- Provide all the files related to the solution in one zip file, the solution should work directly after extracting it on a server without the need to modify anything in the files to make it work.


# Notes:
- At the first sight, as solution requirements that you didn't ask to use Laravel, then I'm too confuse and tried to implement it via simple HTML and PHP, then I must change it to Laravel. That's why it took longer than 1 hours. I know it was late but I just want to complete and send it to you.


Thank you for waiting and reminded by email.

minh.dinhvq@gmail.com


