<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('coalitiontechnologies',['as' => 'product.index', 'uses' => 'ProductController@index']);
Route::post('coalitiontechnologies/submit',['as' => 'product.submit', 'uses' => 'ProductController@createOrEdit']);